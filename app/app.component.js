System.register(['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent() {
                    var _this = this;
                    var startTime = Date.now();
                    this.add(2, 3)
                        .then(function (result) { return _this.add(result, 4); })
                        .then(function (result) { return _this.add(result, 10); })
                        .then(function (result) { return _this.result = result; })
                        .catch(function (error) { return _this.error = error; })
                        .then(function () { return _this.time = Date.now() - startTime; });
                }
                AppComponent.prototype.add = function (x, y) {
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            var result = x + y;
                            if (result >= 0) {
                                resolve(x + y);
                            }
                            else {
                                reject('Nieprawidłowa wartość : ' + result);
                            }
                        }, 1000);
                    });
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        template: "\n     <p>Result: {{result}}</p>\n     <p>Time: {{time}}</p>\n     <p>Error: {{error}}</p>\n  "
                    }), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
